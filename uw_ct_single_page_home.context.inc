<?php

/**
 * @file
 * uw_ct_single_page_home.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_single_page_home_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'responsive_menu';
  $context->description = 'Adds responsive menu';
  $context->tag = 'Navigation';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'responsive_menu_combined-responsive_menu_combined' => array(
          'module' => 'responsive_menu_combined',
          'delta' => 'responsive_menu_combined',
          'region' => 'global_header',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Adds responsive menu');
  t('Navigation');
  $export['responsive_menu'] = $context;

  return $export;
}
