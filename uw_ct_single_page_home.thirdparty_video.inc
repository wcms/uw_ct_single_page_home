<?php

/**
 * @file
 * uw_ct_single_page_home.slideshow.inc
 */

/**
 * Implements hook_permission().
 */
function uw_ct_single_page_home_permission() {
  return array(
    'administer third party video configuration' => array(
      'title' => t('Administer third party video configuration'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function uw_ct_single_page_home_menu() {
  $items['admin/config/system/uw_ct_single_page_home_settings'] = array(
    'title' => 'Third party video banner settings',
    'description' => 'Enable use of third-party video banner using a Vimeo paid account.',
    'page callback' => 'drupal_get_form',
    'type' => MENU_NORMAL_ITEM,
    'menu_name' => 'menu-site-management',
    'page arguments' => array('uw_ct_single_page_home_settings_form'),
    'access arguments' => array('administer third party video configuration'),
  );
  return $items;
}

/**
 * Menu callback; Config form.
 */
function uw_ct_single_page_home_settings_form($form, &$form_state) {
  $form['uw_thirdparty_banner_paid_vimeo'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable third party video banners using a paid Vimeo account'),
    '#default_value' => variable_get('uw_thirdparty_banner_paid_vimeo', 0),
    '#description' => t('ONLY enable this if you will be using a video from a paid Vimeo account. Videos from free accounts will display the video player and other Vimeo interface elements over the video.'),
  );
  return system_settings_form($form);
}
