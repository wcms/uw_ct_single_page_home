<?php

/**
 * @file
 * uw_ct_single_page_home.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_single_page_home_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_banner|node|uw_ct_single_page_home|form';
  $field_group->group_name = 'group_banner';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_ct_single_page_home';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Banner',
    'weight' => '7',
    'children' => array(
      0 => 'field_banner_description',
      1 => 'field_banner_type',
      2 => 'field_banner_video',
      3 => 'field_uw_sph_banner_image',
      4 => 'field_banner_fallback_image',
      5 => 'field_third_party_video',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-banner field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_banner|node|uw_ct_single_page_home|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_event_details|paragraphs_item|sph_remote_events|form';
  $field_group->group_name = 'group_event_details';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sph_remote_events';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Show/hide event details',
    'weight' => '10',
    'children' => array(
      0 => 'field_sph_event_date',
      1 => 'field_event_teaser',
      2 => 'field_event_grades',
      3 => 'field_event_types',
      4 => 'field_sph_event_cost',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Show/hide event details',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_event_details|paragraphs_item|sph_remote_events|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_events|paragraphs_item|sph_remote_events_block|form';
  $field_group->group_name = 'group_events';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sph_remote_events_block';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Show/hide events',
    'weight' => '2',
    'children' => array(
      0 => 'field_events',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Show/hide events',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_events|paragraphs_item|sph_remote_events_block|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_file|node|uw_ct_single_page_home|form';
  $field_group->group_name = 'group_file';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_ct_single_page_home';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload a file',
    'weight' => '10',
    'children' => array(
      0 => 'field_uw_sph_file',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Upload a file',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_file|node|uw_ct_single_page_home|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sh_generic_event_details|paragraphs_item|sph_generic_remote_events|form';
  $field_group->group_name = 'group_sh_generic_event_details';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sph_generic_remote_events';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Show/hide details',
    'weight' => '5',
    'children' => array(
      0 => 'field_sph_generic_event_date',
      1 => 'field_generic_event_teaser',
      2 => 'field_generic_event_cost',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Show/hide details',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_sh_generic_event_details|paragraphs_item|sph_generic_remote_events|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_show_hide_copy|paragraphs_item|copy_block|form';
  $field_group->group_name = 'group_show_hide_copy';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'copy_block';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Show/hide copy',
    'weight' => '3',
    'children' => array(
      0 => 'field_sph_copy',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-show-hide-copy field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_show_hide_copy|paragraphs_item|copy_block|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_show_hide_event_details|paragraphs_item|sph_remote_events|form';
  $field_group->group_name = 'group_show_hide_event_details';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sph_remote_events';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Show/hide details',
    'weight' => '2',
    'children' => array(
      0 => 'field_event_teaser',
      1 => 'field_sph_event_cost',
      2 => 'field_event_grades',
      3 => 'field_sph_event_date',
      4 => 'field_sph_event_themes',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Show/hide details',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_show_hide_event_details|paragraphs_item|sph_remote_events|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_show_hide_link_details|paragraphs_item|uw_para_link_block|form';
  $field_group->group_name = 'group_show_hide_link_details';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'uw_para_link_block';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Show/hide details',
    'weight' => '1',
    'children' => array(
      0 => 'field_sph_link_description',
      1 => 'field_sph_background_image',
      2 => 'field_sph_colouring',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-show-hide-link-details field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_show_hide_link_details|paragraphs_item|uw_para_link_block|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_show_hide_marketing|paragraphs_item|sph_marketing_block|form';
  $field_group->group_name = 'group_show_hide_marketing';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sph_marketing_block';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Show/hide marketing items',
    'weight' => '3',
    'children' => array(
      0 => 'field_marketing_items',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-show-hide-marketing field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_show_hide_marketing|paragraphs_item|sph_marketing_block|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_show_hide_mkt_content|paragraphs_item|sph_marketing_item|form';
  $field_group->group_name = 'group_show_hide_mkt_content';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sph_marketing_item';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Show/hide content',
    'weight' => '2',
    'children' => array(
      0 => 'field_marketing_item_content',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Show/hide content',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_show_hide_mkt_content|paragraphs_item|sph_marketing_item|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_show_hide_options|paragraphs_item|uw_para_options_blocks|form';
  $field_group->group_name = 'group_show_hide_options';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'uw_para_options_blocks';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Show/hide options',
    'weight' => '2',
    'children' => array(
      0 => 'field_sph_options',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-show-hide-options field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_show_hide_options|paragraphs_item|uw_para_options_blocks|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_show_hide_quicklinks|paragraphs_item|sph_quicklinks_block|form';
  $field_group->group_name = 'group_show_hide_quicklinks';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sph_quicklinks_block';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Show/hide quicklinks',
    'weight' => '3',
    'children' => array(
      0 => 'field_sph_quicklink',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-show-hide-quicklinks field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_show_hide_quicklinks|paragraphs_item|sph_quicklinks_block|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sph_non_wcms_event_details|paragraphs_item|sph_non_wcms_event|form';
  $field_group->group_name = 'group_sph_non_wcms_event_details';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sph_non_wcms_event';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Show/hide details',
    'weight' => '4',
    'children' => array(
      0 => 'field_sph_non_wcms_event_date',
      1 => 'field_sph_non_wcms_event_teaser',
      2 => 'field_sph_non_wcms_event_cost',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-sph-non-wcms-event-details field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_sph_non_wcms_event_details|paragraphs_item|sph_non_wcms_event|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_upload|node|uw_ct_single_page_home|form';
  $field_group->group_name = 'group_upload';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_ct_single_page_home';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload an image',
    'weight' => '9',
    'children' => array(
      0 => 'field_uw_sph_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Upload an image',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_upload|node|uw_ct_single_page_home|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Banner');
  t('Show/hide content');
  t('Show/hide copy');
  t('Show/hide details');
  t('Show/hide event details');
  t('Show/hide events');
  t('Show/hide marketing items');
  t('Show/hide options');
  t('Show/hide quicklinks');
  t('Upload a file');
  t('Upload an image');

  return $field_groups;
}
