<?php

/**
 * @file
 * uw_ct_single_page_home.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function uw_ct_single_page_home_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Engineering',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 3,
    'uuid' => '071c6cb9-04b2-4b3b-9e92-427453258242',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'stem_event_types',
    'field_sph_colouring' => array(
      'und' => array(
        0 => array(
          'value' => 'eng',
        ),
      ),
    ),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
  );
  $terms[] = array(
    'name' => 'Mathematics',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 5,
    'uuid' => '6c58de07-5d20-4dc1-b35f-a32a46e3a393',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'stem_event_types',
    'field_sph_colouring' => array(
      'und' => array(
        0 => array(
          'value' => 'math',
        ),
      ),
    ),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
  );
  $terms[] = array(
    'name' => 'Science',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '81eb6512-d53c-49d2-abcc-f01e9f3ec6be',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'stem_event_types',
    'field_sph_colouring' => array(
      'und' => array(
        0 => array(
          'value' => 'sci',
        ),
      ),
    ),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
  );
  $terms[] = array(
    'name' => 'Arts',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 2,
    'uuid' => 'a0885edd-e2df-4846-90ba-dad98261250b',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'stem_event_types',
    'field_sph_colouring' => array(
      'und' => array(
        0 => array(
          'value' => 'arts',
        ),
      ),
    ),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
  );
  $terms[] = array(
    'name' => 'Computer Science',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 1,
    'uuid' => 'a2a6acf8-6c17-4f08-952b-14dc8864690a',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'stem_event_types',
    'field_sph_colouring' => array(
      'und' => array(
        0 => array(
          'value' => 'math',
        ),
      ),
    ),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
  );
  $terms[] = array(
    'name' => 'Environment',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 4,
    'uuid' => 'a8ea879a-2ccb-4d0e-91b7-b057f687061d',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'stem_event_types',
    'field_sph_colouring' => array(
      'und' => array(
        0 => array(
          'value' => 'env',
        ),
      ),
    ),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
  );
  $terms[] = array(
    'name' => 'Health',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 6,
    'uuid' => 'b394dfc6-1557-4b7c-bb28-aabb19777de6',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'stem_event_types',
    'field_sph_colouring' => array(
      'und' => array(
        0 => array(
          'value' => 'ahs',
        ),
      ),
    ),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
  );
  return $terms;
}
