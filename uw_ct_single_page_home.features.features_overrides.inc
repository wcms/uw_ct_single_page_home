<?php

/**
 * @file
 * uw_ct_single_page_home.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_ct_single_page_home_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: image
  $overrides["image.banner_large.effects|0|data|focal_point_advanced"] = array(
    'shift_x' => '',
    'shift_y' => '',
  );
  $overrides["image.banner_large.effects|0|name"] = 'focal_point_scale_and_crop';
  $overrides["image.banner_large.effects|1"]["DELETED"] = TRUE;
  $overrides["image.banner_med.effects|0|data|focal_point_advanced"] = array(
    'shift_x' => '',
    'shift_y' => '',
  );
  $overrides["image.banner_med.effects|0|name"] = 'focal_point_scale_and_crop';
  $overrides["image.banner_small.effects|0|data|focal_point_advanced"] = array(
    'shift_x' => '',
    'shift_y' => '',
  );
  $overrides["image.banner_small.effects|0|name"] = 'focal_point_scale_and_crop';
  $overrides["image.banner_xl.effects|0|data|focal_point_advanced"] = array(
    'shift_x' => '',
    'shift_y' => '',
  );
  $overrides["image.banner_xl.effects|0|data|upscale"]["DELETED"] = TRUE;
  $overrides["image.banner_xl.effects|0|name"] = 'focal_point_scale_and_crop';
  $overrides["image.banner_xl.effects|0|weight"] = 2;

  return $overrides;
}
