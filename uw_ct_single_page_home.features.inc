<?php

/**
 * @file
 * uw_ct_single_page_home.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_single_page_home_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_styles_alter().
 */
function uw_ct_single_page_home_image_styles_alter(&$data) {
  if (isset($data['banner_large'])) {

    if (!isset($data['banner_large']['storage']) || $data['banner_large']['storage'] == IMAGE_STORAGE_DEFAULT) {
      $data['banner_large']['effects'] = array_values($data['banner_large']['effects']);
      $data['banner_large']['effects'][0]['data']['focal_point_advanced'] = array(
        'shift_x' => '',
        'shift_y' => '',
      );
    }

    if (!isset($data['banner_large']['storage']) || $data['banner_large']['storage'] == IMAGE_STORAGE_DEFAULT) {
      $data['banner_large']['effects'] = array_values($data['banner_large']['effects']);
      $data['banner_large']['effects'][0]['name'] = 'focal_point_scale_and_crop';
    }

    if (!isset($data['banner_large']['storage']) || $data['banner_large']['storage'] == IMAGE_STORAGE_DEFAULT) {
      unset($data['banner_large']['effects'][1]);
    }
  }
  if (isset($data['banner_med'])) {

    if (!isset($data['banner_med']['storage']) || $data['banner_med']['storage'] == IMAGE_STORAGE_DEFAULT) {
      $data['banner_med']['effects'] = array_values($data['banner_med']['effects']);
      $data['banner_med']['effects'][0]['data']['focal_point_advanced'] = array(
        'shift_x' => '',
        'shift_y' => '',
      );
    }

    if (!isset($data['banner_med']['storage']) || $data['banner_med']['storage'] == IMAGE_STORAGE_DEFAULT) {
      $data['banner_med']['effects'] = array_values($data['banner_med']['effects']);
      $data['banner_med']['effects'][0]['name'] = 'focal_point_scale_and_crop';
    }
  }
  if (isset($data['banner_small'])) {

    if (!isset($data['banner_small']['storage']) || $data['banner_small']['storage'] == IMAGE_STORAGE_DEFAULT) {
      $data['banner_small']['effects'] = array_values($data['banner_small']['effects']);
      $data['banner_small']['effects'][0]['data']['focal_point_advanced'] = array(
        'shift_x' => '',
        'shift_y' => '',
      );
    }

    if (!isset($data['banner_small']['storage']) || $data['banner_small']['storage'] == IMAGE_STORAGE_DEFAULT) {
      $data['banner_small']['effects'] = array_values($data['banner_small']['effects']);
      $data['banner_small']['effects'][0]['name'] = 'focal_point_scale_and_crop';
    }
  }
  if (isset($data['banner_xl'])) {

    if (!isset($data['banner_xl']['storage']) || $data['banner_xl']['storage'] == IMAGE_STORAGE_DEFAULT) {
      $data['banner_xl']['effects'] = array_values($data['banner_xl']['effects']);
      $data['banner_xl']['effects'][0]['data']['focal_point_advanced'] = array(
        'shift_x' => '',
        'shift_y' => '',
      );
    }

    if (!isset($data['banner_xl']['storage']) || $data['banner_xl']['storage'] == IMAGE_STORAGE_DEFAULT) {
      $data['banner_xl']['effects'] = array_values($data['banner_xl']['effects']);
      $data['banner_xl']['effects'][0]['name'] = 'focal_point_scale_and_crop';
    }

    if (!isset($data['banner_xl']['storage']) || $data['banner_xl']['storage'] == IMAGE_STORAGE_DEFAULT) {
      $data['banner_xl']['effects'] = array_values($data['banner_xl']['effects']);
      $data['banner_xl']['effects'][0]['weight'] = 2;
    }

    if (!isset($data['banner_xl']['storage']) || $data['banner_xl']['storage'] == IMAGE_STORAGE_DEFAULT) {
      unset($data['banner_xl']['effects'][0]['data']['upscale']);
    }
  }
}

/**
 * Implements hook_image_default_styles().
 */
function uw_ct_single_page_home_image_default_styles() {
  $styles = array();

  // Exported image style: banner_large.
  $styles['banner_large'] = array(
    'label' => 'banner_large',
    'effects' => array(
      0 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1000,
          'height' => 600,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: banner_med.
  $styles['banner_med'] = array(
    'label' => 'banner_med',
    'effects' => array(
      0 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 800,
          'height' => 400,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: banner_small.
  $styles['banner_small'] = array(
    'label' => 'banner_small',
    'effects' => array(
      0 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 480,
          'height' => 480,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: banner_xl.
  $styles['banner_xl'] = array(
    'label' => 'banner_xl',
    'effects' => array(
      0 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1600,
          'height' => 553,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: sph_header_image_large.
  $styles['sph_header_image_large'] = array(
    'label' => 'SPH header image large',
    'effects' => array(
      5 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1024,
          'height' => 300,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: sph_header_image_medium.
  $styles['sph_header_image_medium'] = array(
    'label' => 'SPH header image medium',
    'effects' => array(
      6 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 769,
          'height' => 250,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: sph_header_image_small.
  $styles['sph_header_image_small'] = array(
    'label' => 'SPH header image small',
    'effects' => array(
      7 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 480,
          'height' => 200,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: sph_header_image_xlarge.
  $styles['sph_header_image_xlarge'] = array(
    'label' => 'SPH header image xlarge',
    'effects' => array(
      4 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1280,
          'height' => 300,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: uw_is_single_page_captioned_image.
  $styles['uw_is_single_page_captioned_image'] = array(
    'label' => 'Single page captioned image',
    'effects' => array(
      1 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1200,
          'height' => 400,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function uw_ct_single_page_home_node_info() {
  $items = array(
    'uw_ct_single_page_home' => array(
      'name' => t('Single page home'),
      'base' => 'node_content',
      'description' => t('Edit the home page of your single page site.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('Edit the home page of your single page site.'),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function uw_ct_single_page_home_paragraphs_info() {
  $items = array(
    'copy_block' => array(
      'name' => 'Copy block',
      'bundle' => 'copy_block',
      'locked' => '1',
    ),
    'sph_captioned_image_block' => array(
      'name' => 'Captioned image block',
      'bundle' => 'sph_captioned_image_block',
      'locked' => '1',
    ),
    'sph_generic_remote_events' => array(
      'name' => 'WCMS remote events',
      'bundle' => 'sph_generic_remote_events',
      'locked' => '1',
    ),
    'sph_heading_block' => array(
      'name' => 'Heading block',
      'bundle' => 'sph_heading_block',
      'locked' => '1',
    ),
    'sph_image_block' => array(
      'name' => 'Image block',
      'bundle' => 'sph_image_block',
      'locked' => '1',
    ),
    'sph_marketing_block' => array(
      'name' => 'Marketing block',
      'bundle' => 'sph_marketing_block',
      'locked' => '1',
    ),
    'sph_marketing_item' => array(
      'name' => 'Marketing item',
      'bundle' => 'sph_marketing_item',
      'locked' => '1',
    ),
    'sph_navigation_block' => array(
      'name' => 'Navigation block',
      'bundle' => 'sph_navigation_block',
      'locked' => '1',
    ),
    'sph_non_wcms_event' => array(
      'name' => 'Non WCMS event',
      'bundle' => 'sph_non_wcms_event',
      'locked' => '1',
    ),
    'sph_quicklinks_block' => array(
      'name' => 'Quicklinks block',
      'bundle' => 'sph_quicklinks_block',
      'locked' => '1',
    ),
    'sph_remote_events' => array(
      'name' => 'WCMS remote Girls in STEM events',
      'bundle' => 'sph_remote_events',
      'locked' => '1',
    ),
    'sph_remote_events_block' => array(
      'name' => 'Events block',
      'bundle' => 'sph_remote_events_block',
      'locked' => '1',
    ),
    'sph_stories_block' => array(
      'name' => 'Stories block',
      'bundle' => 'sph_stories_block',
      'locked' => '1',
    ),
    'sph_sub_block_navigation_item' => array(
      'name' => 'Navigation Item',
      'bundle' => 'sph_sub_block_navigation_item',
      'locked' => '1',
    ),
    'sph_sub_block_story' => array(
      'name' => 'Story',
      'bundle' => 'sph_sub_block_story',
      'locked' => '1',
    ),
    'uw_para_link_block' => array(
      'name' => 'Options',
      'bundle' => 'uw_para_link_block',
      'locked' => '1',
    ),
    'uw_para_options_blocks' => array(
      'name' => 'Options block',
      'bundle' => 'uw_para_options_blocks',
      'locked' => '1',
    ),
    'uw_para_quicklinks' => array(
      'name' => 'Quicklinks',
      'bundle' => 'uw_para_quicklinks',
      'locked' => '1',
    ),
  );
  return $items;
}

/**
 * Implements hook_rdf_default_mappings().
 */
function uw_ct_single_page_home_rdf_default_mappings() {
  $schemaorg = array();

  // Exported RDF mapping: uw_ct_single_page_home.
  $schemaorg['node']['uw_ct_single_page_home'] = array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  );

  // Exported RDF mapping: stem_event_types.
  $schemaorg['taxonomy_term']['stem_event_types'] = array(
    'rdftype' => array(
      0 => 'skos:Concept',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'rdfs:label',
        1 => 'skos:prefLabel',
      ),
    ),
    'description' => array(
      'predicates' => array(
        0 => 'skos:definition',
      ),
    ),
    'vid' => array(
      'predicates' => array(
        0 => 'skos:inScheme',
      ),
      'type' => 'rel',
    ),
    'parent' => array(
      'predicates' => array(
        0 => 'skos:broader',
      ),
      'type' => 'rel',
    ),
  );

  return $schemaorg;
}
